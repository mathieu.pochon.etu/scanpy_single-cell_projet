Scanpy : https://scanpy.readthedocs.io/en/stable/index.html

From Anaconda
conda install -c conda-forge scanpy python-igraph leidenalg

From PyPI : 
pip install scanpy



generator key (putty) https://www.puttygen.com/

VM initialisation : https://docs.oracle.com/cd/E56338_01/html/E56869/dashboard-createvm.html


ssh-keygen -t rsa -f cloud.key
cat cloud.key.pub

ssh root@floating-ip-address #connection

à tester pour gerer le problèmes ssh :
https://windowsreport.com/windows-11-ssh-permission-denied/



slide template : 
https://slidesgo.com/new-theme/biology-subject-for-middle-school-8th-grade-bioethics#search-biology&position-2&results-132&rs=search
https://slidesgo.com/new-theme/dna-the-human-body-recipe#search-biology&position-31&results-132&rs=search
https://slidesgo.com/new-theme/dna-the-human-body-recipe-infographics#search-biology&position-3&results-132&rs=search
https://slidesgo.com/new-theme/biology-basics-workshop#search-biology&position-27&results-132&rs=search
https://slidesgo.com/new-theme/biology-thesis#search-biology&position-29&results-132&rs=search
https://slidesgo.com/new-theme/biochemical-technician-cv#search-biology&position-36&results-132&rs=search




# executer scanpy en temps d'administrateur


QC : scanpy.pp.calculate_qc_metrics()

Normalization : scanpy.pp.normalize_total()

PCA : scanpy.pp.pca()

clustering : scanpy.external.tl.phenograph()

UMAP : scanpy.pl.umap()

conda install -c conda-forge leidenalg
#pour les umap 


