import scanpy as sc
# pp = preprocessing, pl = plotting
import numpy as np
import pandas as pd


# Téléchargement des données 
adata = sc.read('./Data/matrix.mtx', var_names='./Data/genes.tsv', obs_names='./Data/barcodes.tsv')
print(adata, '\n', type(adata), '\n', adata.shape)

#visualisation des objets chargés identifier les localisations des 

#observation des adn mito

sc.pp.filter_cells(adata, min_genes=200) #filtrage des cell en fonction de présence minimal (comptage) ici 200 gène compté pour une cellule -> goutte vide mais contaminé
#on ne peut pas ce servir des gènes trouver dans des gouttes "vides" pour clean encore les données la dessus
#mettre un max et définir les seuils min et max par observation des violon plots pour couper les queus


sc.pp.filter_genes(adata, min_cells=3) # filtre su les gènes si ils sont présent sur au moins 3 cellules ici

sc.pp.normalize_per_cell(adata) #normalisation avec les paramètres par défault
sc.pp.log1p(adata) #normalisation des valeurs par l'application d'un log nep sur les valerus peut être facultatif 
# ici facultatif comme on a retirer les zéros avec les filtres par seuil minimum pour les gènes et cellules 
print(adata)

sc.pp.scale(adata) #normalisation centré réduite peut être facultatif

sc.pp.highly_variable_genes(adata, n_top_genes=2000)

sc.pl.highly_variable_genes(adata)
# PCA
sc.tl.pca(adata)
sc.pl.pca(adata, colorbar_loc='right', title='PCA on adata set') #changer la forme du graphe avec les repères

#faire le graph des proches 
"""
# Clustering
sc.tl.louvain(adata)
sc.pl.umap(adata, color='louvain')

# UMAP
sc.tl.umap(adata)
sc.pl.umap(adata)
"""

